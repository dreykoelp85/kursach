@extends('admin.app.schema')
<?php
$genre_model = new App\Models\genre_list();
$genres = $genre_model->genres();
?>
@section('title','Add')

@section('content')
    <form action="{{route('films.store')}}" method="post">
        @csrf
        <p>
            <label for="genre_id">Жанр</label>
            <select name="genre_id" id="genre_id">
                @foreach($genres as $genre)
                    <option value='{{ $genre->genre_id }}'>{{ $genre->genre_name }}</option>
                @endforeach
            </select>
        </p>
        <p>
            <label for="name">Назва фільму:</label>
            <input name="name" id="name" type="text">
        </p>
        <p>
            <label for="country">Країна виробництва:</label>
            <input name="country" id="country" type="text">
        </p>
        <p>
            <label for="year">Рік випуску:</label>
            <input name="year" id="year" type="text">
        </p>
        <p>
            <button type="submit">Додати</button>
            <a href="{{route('films.index')}}">Назад</a>
        </p>
    </form>
@endsection
