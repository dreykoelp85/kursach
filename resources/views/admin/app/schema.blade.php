<!doctype html>
<html lang="ua">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
<div>
    <center><h1>Панель адміністратора кінотеатру</h1></center>
    <center>
    @yield('content')

    </center>
</div>
</body>
</html>
