@extends('admin.app.schema')

@section('title','Admin main')

@section('content')
    <p>
        <a href="{{route('films.create')}}">Додати фільм</a>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            {{ __('Вийти') }}
        </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    </p>
    @if(isset($result))
        <table>
            <thead>
            <tr>
                <th>Назва фільму</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td style="width: 200px">
                        <a href="{{route('films.show' , ['id' => $row->id])}}">Інфо</a>
                        |
                        <a href="{{route('films.edit', ['id' => $row->id])}}">Змінити</a>
                        |
                        <form action="{{ route('films.destroy' , ['id'=> $row->id])}}" method="POST">
                            <input name="_method" type="hidden" value="DELETE">
                            {{ csrf_field() }}
                            <button type="submit">Видалити</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
