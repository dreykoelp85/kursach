@foreach($result as $row)
    @extends('admin.app.schema')

    @section('title',$row->name)
@section('content')
    @if(isset($result))
        <table>
            <thead>
            <tr>
                <th>Назва фільму</th>
                <th>Жанр</th>
                <th>Країна виробництва</th>
                <th>Рік виходу</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->genre_name}}</td>
                    <td>{{$row->country}}</td>
                    <td>{{$row->year}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <p>
            <a href="{{route('films.edit',['id' => $row->id])}}">Редагувати</a>
        <form action="{{ route('films.destroy' , ['id'=> $row->id])}}" method="POST">
            <input name="_method" type="hidden" value="DELETE">
            {{ csrf_field() }}
            <button type="submit">Видалити</button>
        </form>
        </p>
    @endif
@endsection
@endforeach
