@extends('admin.app.schema')
@foreach($result as $row)
    <?php
    $genre_model = new App\Models\genre_list();
    $genres = $genre_model->genres();
    ?>
    @section('title','Edit')

@section('content')
    <form action="{{route('films.update',['id' => $row->id])}}"method="post">
        <input name="_method" type="hidden" value="PUT">
        @csrf
        <p>
            <label for="genre_id">Жанр</label>
            <select name="genre_id" id="genre_id">
                @foreach($genres as $genre)
                    <option value='{{ $genre->genre_id }}'>{{ $genre->genre_name }}</option>
                @endforeach
            </select>
        </p>
        <p>
            <label for="name">Назва фільму:</label>
            <input name="name" id="name" type="text" value="{{$row->name}}">
        </p>
        <p>
            <label for="country">Країна виробництва:</label>
            <input name="country" id="country" type="text"  value="{{$row->country}}">
        </p>
        <p>
            <label for="year">Рік випуску:</label>
            <input name="year" id="year" type="text"  value="{{$row->year}}">
        </p>
        <p>
            <button type="submit">Редагувати</button>
            <a href="{{route('films.index')}}">Назад</a>
        </p>
    </form>
@endsection
@endforeach
