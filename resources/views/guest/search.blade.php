@extends('guest.app.schema')

@section('title','Search')

@section('content')
    <form action="{{route('search-form')}}" method="post">
        @csrf
        <label for="name">Назва фільма:</label>
        <input type="text" id="name" name="name">
        <p>
            <button type="submit">Знайти</button>
            <a href="{{route('index')}}">На головну</a>
        </p>
    </form>
@endsection
