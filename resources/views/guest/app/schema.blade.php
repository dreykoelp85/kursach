<!doctype html>
<html lang="ua">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
<div>
    <center><h1>Сайт кінотеатру</h1></center>
    <center>
        @yield('content')
        @if(isset($genres))
            @foreach($genres as $genre)
                <a href="{{route('sorting',['name' => $genre->genre_name])}}">{{$genre->genre_name}}</a>
            @endforeach
        @endif
        @if(isset($result))
            <table>
                <thead>
                <tr>
                    <th>Назва фільму</th>
                    <th>Жанр</th>
                    <th>Країна виробництва</th>
                    <th>Рік виходу</th>
                </tr>
                </thead>
                <tbody>
                @foreach($result as $row)
                    <tr>
                        <td>{{$row->name}}</td>
                        <td>{{$row->genre_name}}</td>
                        <td>{{$row->country}}</td>
                        <td>{{$row->year}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </center>
</div>
</body>
</html>
