<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CinemaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CinemaController::class, 'index'])->name('index');
Route::get('/genres/{name}', [CinemaController::class, 'sorting'])->name('sorting');

Route::get('/search', function () {
    return view('guest.search');
})->name('search');

Route::post('/search/', [CinemaController::class, 'show'])->name('search-form');


Route::middleware('auth')->group(function () {

    Route::resource('admin/films', 'App\Http\Controllers\AdminController',
        ['parameters' =>[
    'films' => 'id']]
    );

});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Auth::logout();

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

//
