<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Genre_list extends Model
{
    use HasFactory;

    protected $table = "genre_list";

    public function genres()
    {
        return DB::table('genre_list')->get();
    }

    public $timestamps = false;
}
