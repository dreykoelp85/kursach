<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cinema extends Model
{
    use HasFactory;

    protected $table = "cinema";
    public $timestamps = false;

    public function cinema()
    {
        return DB::table('cinema')->get();
    }

    public function index()
    {
        return cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->get();
    }

    public function show($name)
    {
      return cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->where('cinema.name', 'like', "$name%")
            ->get();
    }

    public function id_show($id){
       return cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->where('cinema.id', $id)
            ->get();
    }
}
