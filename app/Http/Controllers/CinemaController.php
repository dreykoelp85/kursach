<?php

namespace App\Http\Controllers;

use App\Models\cinema;
use App\Models\Genre_list;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CinemaController extends Controller
{

    public function index()
    {
        $films = cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->get();
        return view('guest.index', ['result' => $films, 'genres' => (new Genre_list())->genres()]);
    }

    public function sorting($name)
    {
        $films = cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->where('genre_list.genre_name', $name)
            ->get();

        return view('guest.index', ['result' => $films, 'genres' => (new Genre_list())->genres()]);
    }

    public function show(Request $req)
    {
        $name = $req->input('name');
        $films = cinema::select('cinema.*', 'genre_list.*')
            ->join('genre_list', 'genre_list.genre_id', '=', 'cinema.genre_id')
            ->where('cinema.name', 'like', "$name%")
            ->get();

        return view('guest.search', ['result' => $films]);
    }
}


