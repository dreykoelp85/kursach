<?php

namespace App\Http\Controllers;

use App\Models\cinema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = (new Cinema)->index();
        return view('admin.index', ['result' => $films]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $film = new cinema;
        $film->genre_id = $request->input('genre_id');
        $film->name = $request->input('name');
        $film->country = $request->input('country');
        $film->year = $request->input('year');
        $film->save();
        return redirect()->route('films.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $films = (new Cinema)->id_show($id);
        return view('admin.info', ['result' => $films]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $films = (new Cinema)->id_show($id);
        return view('admin.edit', ['result' => $films]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('cinema')
            ->where('id', $id)
            ->update([
                'genre_id' => $request->input('genre_id'),
                'name' => $request->input('name'),
                'country' => $request->input('country'),
                'year' => $request->input('year')]);
        return redirect()->route('films.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        cinema::where('id', $id)->delete();
        return redirect()->route('films.index');
    }
}
